﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public GameObject Flash02;

	bool IsShake = false;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider target)
	{
		if("Cube" == target.tag)
		{
			if( IsShake )
				return;
			
			IsShake = true;
			
			//iTween.ShakePosition (Camera.main.gameObject, iTween.Hash ("x", 0.3f, "y", 0.3f, "time", 0.5f));
			
			GameObject NewFlash02 = Instantiate(Flash02, transform.position, Quaternion.identity);
			Destroy(NewFlash02, 10f);
		}
	}
}

