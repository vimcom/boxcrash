﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GameManager : MonoBehaviour {

	private static GameManager instance; 
	public static GameManager GetInstance()
	{
		if (instance == null)
		{
			instance = FindObjectOfType<GameManager> ();
			if (instance == null)
			{     
				GameObject container = new GameObject ("GameManager"); 
				instance = container.AddComponent<GameManager> ();
			} 
		}
		return instance; 
	}
		
	public GameObject CubeL1;
	public Transform Trans;
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void ClearCube()
	{
		GameObject[] pAllObjects = (GameObject[])Resources.FindObjectsOfTypeAll(typeof(GameObject));

		foreach (GameObject pObject in pAllObjects) 
		{
			if (pObject.name.Equals("Cube(Clone)"))
			{
				Destroy(pObject);
			}
		}
	}
	
	public void SetStage(string _string)
	{
		ClearCube();
		
		List<Dictionary<string, object>> data = CSVReader.Read(_string);
		
		for(int i = 0; i < data.Count; i++)
		{
			//Debug.Log("index " + (i).ToString() + " : " + data[i]["type"] + "," + data[i]["x"] + "," + data[i]["y"] + "," + data[i]["z"]);	
			

			int mass = (int)data[i]["mass"];
			float x = (int)data[i]["x"];
			float y = (float)data[i]["y"];
			float z = (int)data[i]["z"];
			
			Trans.position = new Vector3(x, y, z);
			GameObject NewCube = Instantiate(CubeL1, Trans.position, Quaternion.identity);
			NewCube.GetComponent<Rigidbody>().mass = mass;
			
			Color32 color;
			
			switch(mass)
			{
				case 1: color = new Color32(180, 180, 250, 100); break;
				case 3: color = new Color32(120, 120, 220, 100); break;
				case 7: color = new Color32(20, 20, 100, 100); break;
				case 14: color = new Color32(0, 0, 40, 100); break;
				case 27: color = new Color32(0, 0, 0, 100); break;
				default: color = new Color32(0, 0, 0, 100); break;
			}
			
			NewCube.GetComponent<Renderer>().material.color = color;
		}
	}
	
	public void RandomStage()
	{
		ClearCube();
		
		int nRandom = Random.Range(7, 40);
		
		for(int i = 0; i < nRandom; ++i)
		{
			Trans.position = new Vector3(Random.Range(-4, 5), Random.Range(1, 25), Random.Range(0, 4));
			GameObject NewCube = Instantiate(CubeL1, Trans.position, Quaternion.identity);
			NewCube.GetComponent<Rigidbody>().mass = Random.Range(1, 7);
			NewCube.GetComponent<Renderer>().material.color = new Color32(180, 250, 180, 100);
		}
	}
}
