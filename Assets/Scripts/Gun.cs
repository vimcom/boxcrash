﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

	public GameObject Bullet;
	public float Speed = 3f;
	public float Power = 1000f;
	public float BulletSize = 1;
	
	private Vector3 screenPoint;
	private Vector3 offset;
	
	public GameObject Ring;
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.J))
		{
			Shoot();
		}
		
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		transform.position += new Vector3(h, v, 0) * Speed * Time.deltaTime;
	}

	public void Shoot()
	{
		// Gravity
		// -9.81
			
		// 자식 FirePoint 찾아서 그 위치에 총알 생성
		GameObject FirePoint = transform.Find("FirePoint").gameObject;
		GameObject NewBullet = Instantiate(Bullet, FirePoint.transform.position, Quaternion.identity);
			
		// 총알 크기 설정
		NewBullet.transform.localScale = new Vector3(BulletSize, BulletSize, BulletSize);
			
		// 총알 파워 적용
		Power = 2000f;
		NewBullet.GetComponent<Rigidbody>().AddForce(Vector3.forward * Power);
		
		// 총알 지연 삭제
		Destroy(NewBullet, 10f);
		
		iTween.ShakePosition (gameObject, iTween.Hash ("z", 0.3, "time", 0.1));
		
		GameObject NewRing = Instantiate(Ring, FirePoint.transform.position, Quaternion.identity);
		Destroy(NewRing, 10f);
	}
	
}
