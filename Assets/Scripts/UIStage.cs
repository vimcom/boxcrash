﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIStage : MonoBehaviour {

	public void ClickButtonStage1()
    {
        GameManager.GetInstance().SetStage("Stage1");
        


    }
	
	public void ClickButtonStage2()
    {
        GameManager.GetInstance().SetStage("Stage2");
    }
	
	public void ClickButtonRandomStage()
    {
		GameManager.GetInstance().RandomStage();
    }
}
